package com.levelup.lambda;

public interface Transform {
    int transform(int a, int b);
}
