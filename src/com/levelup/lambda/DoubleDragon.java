package com.levelup.lambda;

public class DoubleDragon {

    private int a;
    private int b;

    public DoubleDragon(int a, int b) {
        this.a = a;
        this.b = b;
    }

    void transform(Transform transform){
        System.out.println(transform.transform(a,b));
    }
}
