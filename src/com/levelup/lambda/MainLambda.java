package com.levelup.lambda;

public class MainLambda {


    public static void main(String[] args) {
        DoubleDragon doubleDragon = new DoubleDragon(10, 20);

        Integer c = 50;

        doubleDragon.transform((a,b)-> a+b+c);
        
    }


}
