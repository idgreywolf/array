package com.levelup.examples;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        Array<Integer> array = new Array<Integer>()
                .add(20)
                .add(10);

        Array<Double> array1 = new Array<Double>()
                .add(0.4)
                .add(0.6);


        System.out.println(Summary.sum(array1,array));

    }
}
