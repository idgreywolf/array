package com.levelup.examples;

public class Array<T> {

    private Object array[];

    Array() {

    }

    Array(int size) {
        array = new Object[size];
    }

    public Array<T> add(T element) {
        Object[] arr1;
        if (array == null) {
            arr1 = new Object[1];
        } else {
            arr1 = new Object[array.length + 1];
            System.arraycopy(array, 0, arr1, 0, array.length);
        }
        arr1[arr1.length-1] = element;
        array = arr1;

        return this;
    }

    public T get(int n) {
        return (T)array[n];
    }

    public void remove(int n) {
        Object arr1[] = new Object[array.length - 1];
        if (n != 0) {
            System.arraycopy(array, 0, arr1, 0, n);
        }

        System.arraycopy(array, n + 1, arr1, n, array.length - n - 1);
        array = arr1;
    }

    public int getSize() {
        return  array.length;
    }

}
