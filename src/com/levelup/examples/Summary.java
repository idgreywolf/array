package com.levelup.examples;

public class Summary {


    public static double sum(Array<? extends Number> array1, Array<? extends Number> array2) {

        double sum = 0;

        for (int i = 0; i < array1.getSize(); i++) {
            sum+=array1.get(i).doubleValue();
        }

        for (int i = 0; i < array2.getSize(); i++) {
            sum+=array2.get(i).doubleValue();
        }
        return sum;
    }
}
